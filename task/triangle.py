def is_triangle(a, b, c):
    return not (a >= b + c or b >= a + c or c >= a + b)
